package com.danledgard;

/**
 * An interface for implementing Hello services.
 */
public interface Hello {

    String hello();
	
}
